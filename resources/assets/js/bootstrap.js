import Echo from 'laravel-echo'
import Auth from './auth';

try {
    window.$ = window.jQuery = require('jquery');
    window.Popper = require('popper.js').default;
    window.Pusher = require('pusher-js');
    window.vapidPublicKey = document.querySelector("meta[name='vapid_public_key']").getAttribute("content");

    Auth.init();

    window.Echo = new Echo({
        broadcaster: 'pusher',
        key: 'e255a87bb69b7149d4b3',
        cluster: 'ap2',
        encrypted: true,
        auth: {
            headers: {
                Authorization: `Bearer ${Auth.api_token}`
            },
        },
    });

    require('bootstrap/dist/js/bootstrap.js');
} catch (e) {console.log(e)}