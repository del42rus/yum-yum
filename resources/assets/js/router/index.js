import Vue from 'vue';
import VueRouter from 'vue-router';
import Register from '../views/auth/Register.vue'
import Login from '../views/auth/Login.vue'
import RecipeIndex from '../views/recipe/Index.vue'
import RecipeAuthor from '../views/recipe/Author.vue'
import RecipeShow from '../views/recipe/Show.vue'
import MyRecipes from '../views/recipe/Mine.vue'
import Favorites from '../views/recipe/Favorites.vue'
import Popular from '../views/recipe/Popular.vue'
import RecipeForm from '../views/recipe/Form.vue'
import Account from '../views/user/Form.vue'
import Notifications from '../views/user/Notifications.vue'
import Subscriptions from '../views/user/Subscriptions.vue'
import NotFound from '../views/NotFound.vue'

Vue.use(VueRouter);

const router = new VueRouter({
    routes: [
        { path: '/', component: RecipeIndex },
        { path: '/popular', component: Popular },
        { path: '/my-recipes', component: MyRecipes },
        { path: '/favorites', component: Favorites },
        { path: '/recipes/create', component: RecipeForm, meta: { mode: 'create' } },
        { path: '/recipes/:id/edit', component: RecipeForm, meta: { mode: 'edit' } },
        { path: '/recipes/:id', component: RecipeShow },
        { path: '/author/:authorId', component: RecipeAuthor},
        { path: '/register', component: Register },
        { path: '/login', component: Login },
        { path: '/account/notifications', component: Notifications },
        { path: '/account/subscriptions', component: Subscriptions },
        { path: '/account', component: Account },
        { path: '/404', component: NotFound },
        { path: '*', redirect: '/404' },
    ],
    linkActiveClass: 'active'
});

export default router;