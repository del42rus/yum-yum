export default {
    message: null,
    type: null,

    setMessage(message, type = 'primary') {
        this.message = message;
        this.type = type
    }
}