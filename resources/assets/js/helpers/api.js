import axios from 'axios'
import Auth from '../auth'

export function post(url, data) {
    return axios({
        method: 'POST',
        url: url,
        data: data,
        headers: {
            'Authorization': `Bearer ${Auth.api_token}`
        }
    })
}

export function get(url) {
    return axios({
        method: 'GET',
        url: url,
        headers: {
            'Authorization': `Bearer ${Auth.api_token}`
        }
    })
}

export function del(url) {
    return axios({
        method: 'DELETE',
        url: url,
        headers: {
            'Authorization': `Bearer ${Auth.api_token}`
        }
    })
}

export function interceptors(callback) {
    axios.interceptors.response.use((response) => {
        return response;
    }, (error) => {
        callback(error);

        return Promise.reject(error)
    })
}