export default {
    api_token: null,
    user_id: null,
    init() {
        this.api_token = localStorage.getItem('api_token');
        this.user_id = localStorage.getItem('user_id');
    },
    persist() {
        localStorage.setItem('api_token', this.api_token);
        localStorage.setItem('user_id', this.user_id);
    },
    set(api_token, user_id) {
        this.api_token = api_token;
        this.user_id = user_id;
        this.persist()
    },
    clear() {
        localStorage.removeItem('api_token');
        localStorage.removeItem('user_id');

        this.api_token = null;
        this.user_id = null
    },
    logged() {
        return !!this.user_id;
    },
}