<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="vapid_public_key" content="{{ config('webpush.vapid.public_key') }}">
    <title>{{ config('app.name') }}</title>
    <link href="https://fonts.googleapis.com/css?family=Lobster" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ mix('css/app.css') }}">
</head>
<body>
    <div id="root"></div>
    <footer class="footer">
        <div class="container-fluid">
            <p class="float-left text-muted">&copy; Yum-Yum 2017, All rights reserved</p>
            <ul class="float-right social-media">
                <li><a href="#"><i class="fa fa-vk"></i></a></li>
                <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                <li><a href="#"><i class="fa fa-youtube"></i></a></li>
                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
            </ul>
        </div>
    </footer>
    <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
</body>
</html>