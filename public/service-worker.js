"use strict";

self.addEventListener("notificationclick", function(event) {
    event.notification.close();

    if (event.notification.data.url) {
        return
    }

    //To open the app after click notification
    event.waitUntil(
        clients.matchAll({
            type: "window"
        })
        .then(function(clientList) {
            for (var i = 0; i < clientList.length; i++) {
                var client = clientList[i];
                if ("focus" in client) {
                    return client.focus();
                }
            }

            if (clientList.length === 0) {
                if (clients.openWindow) {
                    return clients.openWindow(event.notification.data.url);
                }
            }
        })
    );
});

self.addEventListener('push', function(event) {
    event.waitUntil(
        self.registration.pushManager.getSubscription()
            .then(function(subscription) {
                var payload = event.data.json();

                return self.registration.showNotification(payload.title, {
                    body: payload.body.text,
                    icon: payload.icon,
                    tag: payload.url + payload.body + payload.icon + payload.title,
                    data: {
                        url: payload.body.url
                    }
                });
            })
    );
});