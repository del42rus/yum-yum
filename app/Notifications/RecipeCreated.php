<?php

namespace App\Notifications;

use App\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use NotificationChannels\WebPush\WebPushChannel;
use NotificationChannels\WebPush\WebPushMessage;

class RecipeCreated extends Notification
{
    use Queueable;

    protected $recipe;

    public function __construct(Recipe $recipe)
    {
        $this->recipe = $recipe;
    }

    public function via($notifiable)
    {
        return ['database', 'broadcast', WebPushChannel::class];
    }

    public function toArray()
    {
        return [
            'recipe_id' => $this->recipe->id,
            'thumbnail' => $this->recipe->image ? '/imagecache/small/' . $this->recipe->image : '',
            'short_description' => $this->recipe->short_description,
            'date' => $this->recipe->created_at->diffForHumans(),
            'author' => [
                'avatar' => $this->recipe->author->getAvatarUrl(),
                'name' => $this->recipe->author->name,
            ],
            'is_read' => 0
        ];
    }

    public function toWebPush($notifiable, $notification)
    {
        return WebPushMessage::create()
            ->title('New Recipe')
            ->icon($this->recipe->author->avatar ? '/imagecache/avatar/' . $this->recipe->author->avatar : '')
            ->body([
                'text' => $this->recipe->short_description,
                'url' => '/recipes/' . $this->recipe->id
            ]);
    }
}
