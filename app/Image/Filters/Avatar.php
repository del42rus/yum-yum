<?php

namespace App\Image\Filters;

use App\User;
use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Avatar implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(User::AVATAR_WIDTH, User::AVATAR_HEIGHT);
    }
}