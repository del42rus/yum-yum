<?php

namespace App\Image\Filters;

use App\Recipe;
use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;

class Thumbnail implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        return $image->fit(Recipe::THUMBNAIL_WIDTH, Recipe::THUMBNAIL_HEIGHT);
    }
}