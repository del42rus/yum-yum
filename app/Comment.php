<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = ['text', 'recipe_id'];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
