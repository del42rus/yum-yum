<?php

namespace App\Jobs;

use App\Notifications\RecipeCreated;
use App\Recipe;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Notification;

class SendNewRecipeNotification implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $recipe;

    public function __construct(Recipe $recipe)
    {
        $this->recipe = $recipe;
    }

    public function handle()
    {
        $users = $this->recipe->author->subscribers()->wherePivot('enable_notifications', 1)->get();

        Notification::send($users, new RecipeCreated($this->recipe));
    }
}
