<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use NotificationChannels\WebPush\HasPushSubscriptions;

class User extends Authenticatable
{
    use Notifiable;
    use HasPushSubscriptions;

    const AVATAR_DIRECTORY = 'avatars';
    const AVATAR_WIDTH = 60;
    const AVATAR_HEIGHT = 60;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'api_token'
    ];

    public function generateToken()
    {
        $this->api_token = str_random(60);
    }

    public function recipes()
    {
        return $this->hasMany(Recipe::class);
    }

    public function favorites()
    {
        return $this->belongsToMany(Recipe::class, 'favorites');
    }

    public function likes()
    {
        return $this->belongsToMany(Recipe::class, 'likes');
    }

    public function dislikes()
    {
        return $this->belongsToMany(Recipe::class, 'dislikes');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function setAvatarAttribute($value)
    {
        $this->attributes['avatar'] = strpos($value, '/') !== false ? substr($value, strpos($value, '/') + 1) : $value;
    }

    public function getAvatarUrl()
    {
        return $this->avatar ? '/imagecache/avatar/' . $this->avatar : null;
    }

    public function subscribers()
    {
        return $this->belongsToMany(User::class, 'subscriptions', 'author_id', 'user_id');
    }

    public function authors()
    {
        return $this->belongsToMany(User::class, 'subscriptions', 'user_id', 'author_id')->as('subscription')->withPivot('enable_notifications');
    }
}
