<?php

namespace App\Console\Commands;

use App\Recipe;
use App\User;
use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Facades\Storage;

class Reset extends Command
{
    protected $signature = 'reset';

    protected $description = 'Reset application data';

    public function __construct()
    {
        parent::__construct();
    }

    public function handle()
    {
        $this->resetImages();

        $this->call('db:seed');
        $this->call('cache:clear');
    }

    protected function resetImages()
    {
        Storage::disk('public')->deleteDirectory(Recipe::IMAGE_DIRECTORY);
        Storage::disk('public')->deleteDirectory(User::AVATAR_DIRECTORY);

        $filesystem = new Filesystem();

        $images = $filesystem->allFiles(base_path('tests/_data/images'));

        foreach ($images as $image) {
            Storage::disk('public')->put(Recipe::IMAGE_DIRECTORY . '/' . $image->getFilename(), $filesystem->get($image->getPathname()));
        }

        $avatars = $filesystem->allFiles(base_path('tests/_data/avatars'));

        foreach ($avatars as $avatar) {
            Storage::disk('public')->put(User::AVATAR_DIRECTORY . '/' . $avatar->getFilename(), $filesystem->get($avatar->getPathname()));
        }
    }
}
