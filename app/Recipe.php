<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recipe extends Model
{
    const IMAGE_DIRECTORY = 'images';
    const THUMBNAIL_WIDTH = 160;
    const THUMBNAIL_HEIGHT = 160;

    protected $fillable = [
        'name', 'short_description', 'description', 'number_of_servings', 'cooking_time', 'category_id', 'cuisine_id',
        'proteins', 'fats', 'carbs', 'calories'
    ];

    public function author()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function cuisine()
    {
        return $this->belongsTo(Cuisine::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class)->withPivot('quantity', 'unit');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'favorites');
    }

    public function likes()
    {
        return $this->belongsToMany(User::class, 'likes');
    }

    public function dislikes()
    {
        return $this->belongsToMany(User::class, 'dislikes');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    public function getFormattedCookingTime()
    {
        list($hours, $min) = explode(':', $this->cooking_time);

        $formatted = '';

        if ((int) $hours) {
            $formatted = $hours > 1 ? sprintf('%s hours', $hours) : sprintf('%s hour', $hours);
        }

        if ((int) $min) {
            $formatted .= ' ' . sprintf('%s min', $min);
        }

        return trim($formatted);
    }

    public function setImageAttribute($value)
    {
        $this->attributes['image'] = strpos($value, '/') !== false ? substr($value, strpos($value, '/') + 1) : $value;
    }
}
