<?php

namespace App\Http\Controllers\Api;

use App\Cuisine;
use App\Http\Controllers\Controller;
use App\Http\Resources\CuisineCollection;
use Illuminate\Http\Request;

class CuisineController extends Controller
{
    public function index()
    {
        return new CuisineCollection(Cuisine::all());
    }
}
