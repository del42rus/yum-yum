<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PushSubscriptionController extends Controller
{
    public function update(Request $request)
    {
        $this->validate($request, [
            'endpoint' => 'required'
        ]);

        $request->user()->updatePushSubscription(
            $request->endpoint,
            $request->key,
            $request->token
        );

        return response()->json([
            'success' => 1
        ]);
    }

    public function destroy(Request $request)
    {
        $this->validate($request, ['endpoint' => 'required']);
        $request->user()->deletePushSubscription($request->endpoint);

        return response()->json(null, 204);
    }
}
