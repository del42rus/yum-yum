<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Recipe;
use App\User;
use Illuminate\Http\Request;

class SubscriptionController extends Controller
{
    public function index(Request $request)
    {
        $data = [];

        foreach ($request->user()->authors as $author) {
            $data[$author->id] = [
                'author_id' => $author->id,
                'author_name' => $author->name,
                'author_avatar' => $author->getAvatarUrl(),
                'enable_notifications' => $author->subscription->enable_notifications
            ];
        }

        return response()->json([
            'data' => $data
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'author_id' => 'required|exists:users,id',
        ]);

        $author = User::find($request->author_id);
        $request->user()->authors()->attach($author);

        return response()->json([
            'success' => 1,
            'data' => [
                'author_id' => $author->id,
                'author_name' => $author->name,
                'author_avatar' => $author->getAvatarUrl(),
                'enable_notifications' => 0
            ]
        ]);
    }

    public function cancel(Request $request, User $author)
    {
        $request->user()->authors()->detach($author);

        return response()->json([
            'success' => 1
        ]);
    }

    public function update(Request $request, User $author)
    {
        $request->validate([
            'enable_notifications' => 'integer',
        ]);

        $request->user()->authors()->syncWithoutDetaching([
            $author->id => ['enable_notifications' => $request->enable_notifications]
        ]);

        return response()->json([
            'success' => 1
        ]);
    }
}
