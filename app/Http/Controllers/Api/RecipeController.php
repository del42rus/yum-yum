<?php

namespace App\Http\Controllers\Api;

use App\Http\Resources\CommentCollection;
use App\Jobs\SendNewRecipeNotification;
use App\Recipe;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\RecipeCollection;
use App\Http\Resources\Recipe as RecipeResource;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRecipe as StoreRecipeRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class RecipeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['index', 'show', 'popular']);
    }

    public function index()
    {
        return new RecipeCollection(Recipe::paginate(10));
    }

    public function show(Recipe $recipe)
    {
        return new RecipeResource($recipe);
    }

    public function mine(Request $request)
    {
        return new RecipeCollection(Recipe::where('user_id', $request->user()->id)->paginate(10));
    }

    public function author(User $user)
    {
        return new RecipeCollection($user->recipes()->paginate(10));
    }

    public function store(StoreRecipeRequest $request)
    {
        $recipe = new Recipe([
            'name' => $request->input('name'),
            'short_description' => $request->input('short_description'),
            'description' => $request->input('description'),
            'cooking_time' => $request->input('cooking_time'),
            'number_of_servings' => $request->input('number_of_servings'),
            'category_id' => $request->input('category.id'),
            'cuisine_id' => $request->input('cuisine.id'),
            'calories' => $request->input('calories'),
            'proteins' => $request->input('proteins'),
            'fats' => $request->input('fats'),
            'carbs' => $request->input('carbs'),
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            $recipe->image = $request->image->store(Recipe::IMAGE_DIRECTORY, 'public');
        }

        $request->user()->recipes()->save($recipe);

        $ingredients = [];

        foreach ($request->ingredients as $ingredient) {
            $ingredients[$ingredient['id']] = [
                'quantity' => $ingredient['quantity'],
                'unit' => $ingredient['unit']
            ];
        }

        $recipe->ingredients()->attach($ingredients);

        $recipeResource = new RecipeResource($recipe);

        dispatch(new SendNewRecipeNotification($recipe));

        return response()->json([
            'success' => 1,
            'data' => $recipeResource->toArray($request)
        ]);
    }

    public function update(StoreRecipeRequest $request, $id)
    {
        $recipe = $request->user()->recipes()
            ->findOrFail($id);

        $recipe->fill([
            'name' => $request->input('name'),
            'short_description' => $request->input('short_description'),
            'description' => $request->input('description'),
            'cooking_time' => $request->input('cooking_time'),
            'number_of_servings' => $request->input('number_of_servings'),
            'category_id' => $request->input('category.id'),
            'cuisine_id' => $request->input('cuisine.id'),
            'calories' => $request->input('calories'),
            'proteins' => $request->input('proteins'),
            'fats' => $request->input('fats'),
            'carbs' => $request->input('carbs'),
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            if ($recipe->image) {
                Storage::disk('public')->delete(Recipe::IMAGE_DIRECTORY . '/' . $recipe->image);
            }

            $recipe->image = $request->image->store(Recipe::IMAGE_DIRECTORY, 'public');
        }

        $recipe->save();

        $ingredients = [];

        foreach ($request->ingredients as $ingredient) {
            $ingredients[$ingredient['id']] = [
                'quantity' => $ingredient['quantity'],
                'unit' => $ingredient['unit']
            ];
        }

        $recipe->ingredients()->sync($ingredients);

        $recipeResource = new RecipeResource($recipe);

        return response()->json([
            'success' => 1,
            'data' => $recipeResource->toArray($request)
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $recipe = $request->user()->recipes()
            ->findOrFail($id);

        $recipe->ingredients()->detach();
        $recipe->delete();

        return response()
            ->json([
                'success' => 1
            ]);
    }

    public function favorites(Request $request)
    {
        return new RecipeCollection($request->user()->favorites()->paginate(10));
    }

    public function favoritize(Request $request, Recipe $recipe)
    {
        $request->user()->favorites()->syncWithoutDetaching($recipe);

        return response()
            ->json([
                'data' => [
                    'favorite_count' => $recipe->users->count()
                ]
            ]);
    }

    public function unfavoritize(Request $request, Recipe $recipe)
    {
        $request->user()->favorites()->detach($recipe);

        return response()
            ->json([
                'data' => [
                    'favorite_count' => $recipe->users->count()
                ]
            ]);
    }

    public function like(Request $request, Recipe $recipe)
    {
        $request->user()->likes()->toggle($recipe);
        $request->user()->dislikes()->detach($recipe);

        return response()
            ->json([
                'data' => [
                    'likes_count' => $recipe->likes->count(),
                    'dislikes_count' => $recipe->dislikes->count(),
                    'is_liked' => $request->user() && $request->user()->likes->contains($recipe->id),
                    'is_disliked' => $request->user() && $request->user()->dislikes->contains($recipe->id),
                ]
            ]);
    }

    public function dislike(Request $request, Recipe $recipe)
    {
        $request->user()->dislikes()->toggle($recipe);
        $request->user()->likes()->detach($recipe);

        return response()
            ->json([
                'data' => [
                    'likes_count' => $recipe->likes->count(),
                    'dislikes_count' => $recipe->dislikes->count(),
                    'is_liked' => $request->user() && $request->user()->likes->contains($recipe->id),
                    'is_disliked' => $request->user() && $request->user()->dislikes->contains($recipe->id),
                ]
            ]);
    }

    public function popular()
    {
        $recipes = Recipe::select('recipes.*')
            ->join('favorites', 'recipes.id', '=', 'favorites.recipe_id')
            ->groupBy('recipes.id')
            ->orderBy(DB::raw('count(*)'))
            ->paginate(10);

        return new RecipeCollection($recipes);
    }

    public function comments(Recipe $recipe)
    {
        return new CommentCollection($recipe->comments);
    }

    public function thumbnail(Request $request)
    {
        $request->validate([
            'image' => 'image',
        ]);

        if ($request->hasFile('image') && $request->file('image')->isValid()) {
            Storage::disk('public')->makeDirectory(Recipe::IMAGE_DIRECTORY);

            $path = $request->image->store(Recipe::IMAGE_DIRECTORY, 'public');
            $image = Image::make(Storage::disk('public')->path($path));

            Storage::disk('public')->delete($path);

            return response()->json([
                'success' => 1,
                'data' => [
                    'thumbnail' => (string) $image->fit(Recipe::THUMBNAIL_WIDTH, Recipe::THUMBNAIL_HEIGHT)->encode('data-url')
                ]
            ]);
        }

        return response()->json([
            'success' => 0
        ]);
    }


}
