<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use App\Http\Resources\User as UserResource;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Intervention\Image\ImageManagerStatic as Image;

class UserController extends Controller
{
    public function self(Request $request)
    {
        return new UserResource($request->user());
    }

    public function selfUpdate(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'avatar' => 'sometimes|image',
            'email' => [
                'required',
                Rule::unique('users')->ignore($request->user()->id)
            ],
            'password' => 'nullable|min:6|string|confirmed'
        ]);

        $request->user()->fill($request->except(['id', 'password']));

        if ($request->avatar_removed && $request->user()->avatar) {
            Storage::disk('public')->delete(User::AVATAR_DIRECTORY . '/' . $request->user()->avatar);
            $request->user()->avatar = '';
        } else {
            if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
                if ($request->user()->avatar) {
                    Storage::disk('public')->delete(User::AVATAR_DIRECTORY . '/' . $request->user()->avatar);
                }

                $request->user()->avatar = $request->avatar->store(User::AVATAR_DIRECTORY, 'public');
            }
        }

        if ($request->password) {
            $request->user()->password = Hash::make($request->password);
        }

        $request->user()->save();

        return response()->json([
            'success' => 1,
            'message' => 'Profile has been updated successfully',
            'data' => (new UserResource($request->user()))->toArray($request)
        ]);
    }

    public function avatar(Request $request)
    {
        $request->validate([
            'avatar' => 'image',
        ]);

        if ($request->hasFile('avatar') && $request->file('avatar')->isValid()) {
            Storage::disk('public')->makeDirectory(User::AVATAR_DIRECTORY);

            $path = $request->avatar->store(User::AVATAR_DIRECTORY, 'public');
            $avatar = Image::make(Storage::disk('public')->path($path));

            Storage::disk('public')->delete($path);

            return response()->json([
                'success' => 1,
                'data' => [
                    'avatar' => (string) $avatar->fit(User::AVATAR_WIDTH, User::AVATAR_HEIGHT)->encode('data-url')
                ]
            ]);
        }

        return response()->json([
            'success' => 0
        ]);
    }

    public function show(User $user)
    {
        return new UserResource($user);
    }
}
