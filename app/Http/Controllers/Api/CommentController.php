<?php

namespace App\Http\Controllers\Api;

use App\Comment;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Resources\Comment as CommentResource;

class CommentController extends Controller
{
    public function show(Comment $comment)
    {
        return new CommentResource($comment);
    }

    public function store(Request $request)
    {
        $request->validate([
            'text' => 'required',
            'recipe_id' => 'required|exists:recipes,id'
        ]);

        $comment = new Comment($request->all());

        $request->user()->comments()->save($comment);

        return response()->json([
            'success' => 1,
            'data' => (new CommentResource($comment))->toArray($request)
        ]);
    }

    public function update(Request $request, $id)
    {
        $comment = $request->user()->comments()
            ->findOrFail($id);

        $request->validate([
            'text' => 'required',
        ]);

        $comment->text = $request->input('text');
        $comment->save();

        return response()->json([
            'success' => 1,
            'data' => (new CommentResource($comment))->toArray($request)
        ]);
    }

    public function destroy(Request $request, $id)
    {
        $comment = $request->user()->comments()
            ->findOrFail($id);

        $comment->delete();

        return response()
            ->json([
                'success' => 1
            ]);
    }
}
