<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationCollection;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index(Request $request)
    {
        return new NotificationCollection($request->user()->notifications()->limit(5)->get());
    }

    public function markAllRead(Request $request)
    {
        $request->user()
            ->unreadNotifications()
            ->get()->each(function ($notification) {
                $notification->markAsRead();
            });

        return response()->json([
            'success' => 1
        ]);
    }
}
