<?php

namespace App\Http\Resources;

use App\User;
use Illuminate\Http\Resources\Json\Resource;

class Recipe extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        $this->load('ingredients');

        if ($request->bearerToken()) {
            $user = User::where('api_token', $request->bearerToken())->first();
        }

        return [
            'id' => $this->id,
            'name' => $this->name,
            'cuisine' => $this->cuisine,
            'category' => $this->category,
            'short_description' => $this->short_description,
            'description' => $this->description,
            'number_of_servings' => $this->number_of_servings,
            'calories' => $this->calories,
            'proteins' => $this->proteins,
            'fats' => $this->fats,
            'carbs' => $this->carbs,
            'cooking_time' => $this->cooking_time,
            'formatted_cooking_time' => $this->getFormattedCookingTime(),
            'author' => [
                'id' => $this->author->id,
                'name' => $this->author->name,
                'avatar' => $this->author->getAvatarUrl(),
            ],
            'thumbnail' => $this->image ? '/imagecache/thumbnail/' . $this->image : '',
            'medium_image' => $this->image ? '/imagecache/medium/' . $this->image : '',
            'ingredients' => $this->whenLoaded('ingredients', function() {
                $data = [];

                foreach ($this->ingredients as $ingredient) {
                    $data[] = [
                        'id' => $ingredient->id,
                        'name' => $ingredient->name,
                        'quantity' => $ingredient->pivot->quantity,
                        'unit' => $ingredient->pivot->unit
                    ];
                }

                return $data;
            }),
            'favorite_count' => $this->users->count(),
            'is_favorite' => isset($user) && $user->favorites->contains($this->id),
            'likes_count' => $this->likes->count(),
            'dislikes_count' => $this->dislikes->count(),
            'is_liked' => isset($user) && $user->likes->contains($this->id),
            'is_disliked' => isset($user) && $user->dislikes->contains($this->id),
            'comments_count' => $this->comments->count()
        ];
    }
}
