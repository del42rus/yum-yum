<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;
use Carbon\Carbon;

class Comment extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'text' => $this->text,
            'author' => [
                'id' => $this->author->id,
                'name' => $this->author->name,
                'avatar' => $this->author->avatar ? route('imagecache', ['template' => 'avatar', 'filename' => $this->author->avatar]) : '',
            ],
            'date' => $this->created_at->diffForHumans(),
            'edit_mode' => false,
            'edited' => $this->created_at != $this->updated_at
        ];
    }
}
