<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRecipe extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'short_description' => 'required|max:255',
            'description' => 'required',
            'number_of_servings' => 'required|integer',
            'cooking_time' => 'required|regex:/\d{1,2}:\d{2}/',
            'image' => 'required_without:id|image',
            'category.id' => 'required|exists:categories,id',
            'cuisine.id' => 'required|exists:cuisines,id',
            'ingredients' => 'required|array|min:1',
            'ingredients.*.id' => 'required|exists:ingredients,id',
            'ingredients.*.quantity' => 'required|integer|max:255',
            'ingredients.*.unit' => 'required|exists:units,name',
        ];
    }
}
