<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');

Route::group(['middleware' => 'auth:api'], function() {
    Route::post('logout', 'Api\AuthController@logout');
    Route::get('/ingredients', 'Api\IngredientController@index');
    Route::get('/units', 'Api\UnitController@index');
    Route::get('/cuisines', 'Api\CuisineController@index');
    Route::get('/categories', 'Api\CategoryController@index');

    Route::resource('/comments', 'Api\CommentController')->except(['index']);

    Route::get('/users/self', 'Api\UserController@self');
    Route::get('/users/{user}', 'Api\UserController@show');
    Route::put('/users/self', 'Api\UserController@selfUpdate');
    Route::post('/users/avatar', 'Api\UserController@avatar');

    Route::get('/users/self/subscriptions', 'Api\SubscriptionController@index');
    Route::post('/users/self/subscriptions', 'Api\SubscriptionController@store');
    Route::put('/users/self/subscriptions/{author}', 'Api\SubscriptionController@update');
    Route::delete('/users/self/subscriptions/{author}', 'Api\SubscriptionController@cancel');

    Route::get('/users/self/notifications', 'Api\NotificationController@index');
    Route::get('/users/self/notifications/mark-all-read', 'Api\NotificationController@markAllRead');

    Route::post('/users/self/push-subscriptions', 'Api\PushSubscriptionController@update');
    Route::post('/users/self/push-subscriptions/delete', 'Api\PushSubscriptionController@destroy');
});


Route::get('/recipes/popular', 'Api\RecipeController@popular');
Route::resource('/recipes', 'Api\RecipeController');
Route::get('/recipes/{recipe}/favoritize', 'Api\RecipeController@favoritize');
Route::get('/recipes/{recipe}/unfavoritize', 'Api\RecipeController@unfavoritize');
Route::get('/recipes/{recipe}/like', 'Api\RecipeController@like');
Route::get('/recipes/{recipe}/dislike', 'Api\RecipeController@dislike');
Route::get('/recipes/{recipe}/comments', 'Api\RecipeController@comments');
Route::post('/recipes/thumbnail', 'Api\RecipeController@thumbnail');

Route::get('/users/self/recipes', 'Api\RecipeController@mine');
Route::get('/users/self/favorites', 'Api\RecipeController@favorites');
Route::get('/users/{user}/recipes', 'Api\RecipeController@author');

