<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProteinsCarbsFatsCalsFieldsToRecipes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->decimal('proteins')->after('number_of_servings')->nullable();
            $table->decimal('fats')->after('proteins')->nullable();
            $table->decimal('carbs')->after('fats')->nullable();
            $table->decimal('calories')->after('carbs')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('recipes', function (Blueprint $table) {
            $table->dropColumn('proteins');
            $table->dropColumn('fats');
            $table->dropColumn('carbs');
            $table->dropColumn('calories');
        });
    }
}
