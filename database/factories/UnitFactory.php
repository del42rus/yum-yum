<?php

use Faker\Generator as Faker;

$factory->define(App\Unit::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['kg', 'tablespoon', 'tsp.', 'g', 'cup', 'litre', 'millilitre']),
    ];
});
