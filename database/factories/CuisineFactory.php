<?php

use Faker\Generator as Faker;

$factory->define(App\Cuisine::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['Chinese', 'Russian', 'Italian', 'French']),
    ];
});
