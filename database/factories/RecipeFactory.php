<?php

use Faker\Generator as Faker;

$factory->define(App\Recipe::class, function (Faker $faker) {
    return [
        'name' => $faker->sentence,
        'user_id' => $faker->numberBetween(1, 2),
        'category_id' => $faker->numberBetween(1, 4),
        'cuisine_id' => $faker->numberBetween(1, 4),
        'short_description' => $faker->text,
        'description' => $faker->paragraph,
        'number_of_servings' => $faker->numberBetween(1, 4),
        'cooking_time' => $faker->numberBetween(1, 2) . ':' . $faker->randomElement(['00', '10', '20', '30', '40', '50']),
        'proteins' => $faker->numberBetween(10, 25),
        'fats' => $faker->numberBetween(10, 25),
        'carbs' => $faker->numberBetween(10, 25),
        'calories' => $faker->numberBetween(100, 250),
        'image' => $faker->unique()->randomElement(['recipe1.jpg', 'recipe2.jpg', 'recipe3.jpg'])
    ];
});
