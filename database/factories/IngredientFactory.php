<?php

use Faker\Generator as Faker;

$factory->define(App\Ingredient::class, function (Faker $faker) {
    return [
        'name' => $faker->unique()->randomElement(['Flour', 'Oil', 'Sugar', 'Salt', 'Milk', 'Chicken', 'Onion']),
    ];
});
