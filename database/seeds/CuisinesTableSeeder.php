<?php

use Illuminate\Database\Seeder;

class CuisinesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Cuisine::truncate();

        factory(App\Cuisine::class, 4)->create();
    }
}
