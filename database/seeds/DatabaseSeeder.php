<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');

        DB::table('favorites')->truncate();
        DB::table('likes')->truncate();
        DB::table('notifications')->truncate();
        DB::table('jobs')->truncate();
        DB::table('push_subscriptions')->truncate();
        DB::table('subscriptions')->truncate();
        DB::table('comments')->truncate();

        $this->call(UsersTableSeeder::class);
        $this->call(CuisinesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(UnitsTableSeeder::class);
        $this->call(IngredientsTableSeeder::class);
        $this->call(RecipesTableSeeder::class);
        $this->call(IngredientRecipeSeeder::class);

        DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
