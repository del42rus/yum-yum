<?php

use Illuminate\Database\Seeder;

class IngredientRecipeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \Illuminate\Support\Facades\DB::table('ingredient_recipe')->truncate();

        $faker = \Faker\Factory::create();

        $recipes = \App\Recipe::all();
        $ingredientIds = \App\Ingredient::all()->pluck('id')->toArray();
        $unitNames = \App\Unit::all()->pluck('name')->toArray();

        foreach ($recipes as $recipe) {
            $ingredients = [];

            shuffle($ingredientIds);
            $randomIngredientIds = array_slice($ingredientIds, 0, rand(1, count($ingredientIds)));

            foreach ($randomIngredientIds as $ingredientId) {
                $ingredients[$ingredientId] = [
                    'quantity' => $faker->numberBetween(1, 10),
                    'unit' => $faker->randomElement($unitNames)
                ];
            }

            $recipe->ingredients()->sync($ingredients);
        }
    }
}
