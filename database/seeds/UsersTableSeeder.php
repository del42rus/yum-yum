<?php

use App\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::truncate();

        $user = new User();
        $user->name = 'Jane Doe';
        $user->email = 'jane.doe@app.com';
        $user->password = bcrypt('secret');
        $user->api_token = 'api_token';
        $user->remember_token = str_random(10);
        $user->avatar = 'avatar1.jpg';

        $user->save();

        $user = new User();
        $user->name = 'John Doe';
        $user->email = 'john.doe@app.com';
        $user->password = bcrypt('secret');
        $user->api_token = 'api_token2';
        $user->remember_token = str_random(10);
        $user->avatar = 'avatar2.jpg';

        $user->save();
    }
}
